class Abstraction::Component::Glyph < ECS::Component
  property \
    char : Char = '@',
    foreground : UInt32? = 0xffff4020_u32,
    background : UInt32? = nil,
    priority : Int32 = 0
end
