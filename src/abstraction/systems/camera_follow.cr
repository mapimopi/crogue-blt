class Abstraction::System::CameraFollow < ECS::System
  state :gameplay
  filter :camera_focus

  def update(entity)
    camera = @abstraction.camera

    camera.position.x = entity.x
    camera.position.y = entity.y
  end
end