class Abstraction::System::PlayerMovement < ECS::System
  state :gameplay
  filter :player

  def update(entity)
    action = Game.input.action
    return if action == :none

    up    = Set{ :go_n, :go_nw, :go_ne }
    down  = Set{ :go_s, :go_sw, :go_se }
    left  = Set{ :go_w, :go_nw, :go_sw }
    right = Set{ :go_e, :go_ne, :go_se }

    dx = 0
    dy = 0

    dy -= 1 if up.includes? action
    dy += 1 if down.includes? action
    dx -= 1 if left.includes? action
    dx += 1 if right.includes? action

    new_x = entity.x + dx
    new_y = entity.y + dy

    if @abstraction.map.cell_walkable? new_x, new_y
      entity.move_to new_x, new_y
    end
  end
end
