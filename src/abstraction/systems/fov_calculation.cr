class Abstraction::System::FovCalculation < ECS::System
  state :gameplay
  filter :player

  def update(entity)
    @abstraction.map.cell_unsee!

    left   = entity.x - 10
    right  = entity.x + 10
    top    = entity.y - 10
    bottom = entity.y + 10

    ( left..right ).each do |x|
      ( top..bottom ).each do |y|
        @abstraction.map.cell_see x, y
      end
    end
  end
end
