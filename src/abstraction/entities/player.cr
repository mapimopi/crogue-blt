class Abstraction
  def spawn_player
    entity = spawn_at 1, 1

    glyph = Component::Glyph.new
    glyph.char = '@'
    glyph.priority = 100

    entity.add :glyph, glyph
    entity.add :player
    entity.add :camera_focus
  end
end
