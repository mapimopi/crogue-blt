module Abstraction::MapGenerator::BaseFloor
  extend self

  def generate : ECS::MapBlueprint
    width  = 64
    height = 64
    blueprint = ECS::MapBlueprint.new width, height, :cave_wall

    ship_size = 16
    ship_x = i = width / 2 - 9
    ship_y = j = height / 2 - 5
    ship_width = 18 # 15

    width.times do |x|
      height.times do |y|
        next if x == 0 || x == width - 1 ||
                y == 0 || y == height - 1

        blueprint.put x, y, :cave_floor if rand >= 0.5
        blueprint.put x, y, :cave_floor if x > width / 2 - ship_size &&
                                           x < width / 2 + ship_size &&
                                           y > height / 2 - ship_size &&
                                           y < height / 2 + ship_size
      end
    end

    3.times do
      width.times do |x|
        height.times do |y|
          next if x == 0 || x == width - 1 ||
                  y == 0 || y == height - 1

          blueprint.put x, y, wall_logic(blueprint.cells, x, y, width)
        end
      end
    end

    ship.each_char do |ch|
      case ch
      when '.' then blueprint.put i, j, :ship_floor
      when '#' then blueprint.put i, j, :ship_wall
      end

      i += 1
      if i >= ship_x + ship_width
        i = ship_x
        j += 1
      end
    end
    # player_position = Vector2.new width / 2, height / 2

    blueprint
  end

  private def wall_logic(cells, x, y, w)
    count = adjacent_walls cells, x, y, w, 1, 1

    if cells[ index_at x, y, w ] == :cave_wall
      if count >= 4
        return :cave_wall
      elsif count < 2
        return :cave_floor
      end
    else
      return :cave_wall if count >= 5
    end

    return :cave_floor
  end

  private def adjacent_walls(cells, x, y, w, scope_x, scope_y)
    result = 0
    left   = x - 1
    right  = x + 1
    top    = y - 1
    bottom = y + 1

    ( left..right ).each do |i|
      ( top..bottom ).each do |j|
        next if x == i && y == j
        result += 1 if cells[ index_at i, j, w ] == :cave_wall
      end
    end

    result
  end

  private def index_at(x, y, w)
    x * w + y
  end

  private def ship
    "  wwwwwwwwwww  " \
    " wwwwwwwwwwwww " \
    " wwwww...wwwww " \
    " wwwww...wwwww " \
    " wwwwww.wwwwww " \
    " wwww.....wwww " \
    " w..w.www.w..w " \
    " .....www..... " \
    " w..w.www.w..w " \
    " wwww.....wwww " \
    " wwwwww.wwwwww " \
    " w...w...w...w " \
    " w...........w " \
    " w...w...w...w " \
    " wwwwww.wwwwww " \
    "www.........www" \
    "www...www...www" \
    "www...www...www" \
    "www...www...www" \
    "www.........www" \
    "wwwwwwwwwwwwwww" \
    "wwwwww   wwwwww" \
    "wwwwww   wwwwww"
  end

  private def ship
    "  wwwwwwwwwww  " \
    " wwwwwwwwwwwww " \
    " wwwww...wwwww " \
    " wwwww...wwwww " \
    " wwwwww.wwwwww " \
    " wwww.....wwww " \
    " w..w.www.w..w " \
    " .....www..... " \
    " w..w.www.w..w " \
    " wwww.....wwww " \
    " wwww.www.wwww " \
    " ww.........ww " \
    " ww.........ww " \
    " ww.........ww " \
    " wwwwwwwwwwwww " \
    " wwwww   wwwww "
  end

  private def ship
    "####              " \
    "###########.###   " \
    "##............####" \
    " #.###.##.....#..#" \
    " #.###.##........#" \
    " #.###.##.....#..#" \
    "##............####" \
    "###########.###   " \
    "####              "
  end
end
