class Presentation::Gameplay
  private def render_map_view
    camera = Game.abstraction.camera
    map    = Game.abstraction.map

    Terminal.layer Terminal::Layer::Tiles

    width       = Terminal.width
    height      = Terminal.height
    half_width  = width / 2
    half_height = height / 2
    camera_x    = camera.position.x - half_width
    camera_y    = camera.position.y - half_height
    horizontal  = 0 .. width
    vertical    = 0 .. height

    horizontal.each do |x|
      vertical.each do |y|
        cell_x = x + camera_x
        cell_y = y + camera_y

        render_cell map, x, y, cell_x, cell_y
        render_entities map, x, y, cell_x, cell_y
      end
    end
  end

  private def render_cell(map, x, y, cell_x, cell_y)
    # return unless map.cell_seen cell_x, cell_y

    char = map.cell_char cell_x, cell_y
    color = map.cell_color cell_x, cell_y

    unless map.cell_fov cell_x, cell_y
      #color -= 0x80000000_u32
    end

    Terminal.reset_bg_color
    Terminal.put x, y, char, color
  end

  private def render_entities(map, x, y, cell_x, cell_y)
    return unless map.cell_fov cell_x, cell_y

    entities = map.entities_at cell_x, cell_y

    return if entities.empty?

    entities.each do |entity|
      next unless entity.has? :glyph
      glyph = entity.glyph

      char =
        if glyph.priority > 0
          glyph.char
        else
          Terminal.get_char x, y
        end

      foreground =
        if glyph.foreground.nil?
          Terminal.get_fg_color x, y
        else
          glyph.foreground
        end

      background =
        if glyph.background.nil?
          Terminal.get_bg_color x, y
        else
          glyph.background
        end

      Terminal.put x, y, char, foreground.not_nil!, background.not_nil!
    end
  end
end
