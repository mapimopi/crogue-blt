class Presentation::Gameplay < Engine::Presentation
  def initialize
    @last_frame_time = Time.now
  end

  def update
    start_render
    render_map_view
    finish_render
  end

  private def start_render
    Terminal.clear
  end

  private def finish_render
    Terminal.print_text 0, 0, fps

    Terminal.refresh
  end

  private def fps
    current = Time.now
    result = 1.0 / ( current - @last_frame_time ).to_f
    @last_frame_time = current

    return result.to_i.to_s
  end
end
