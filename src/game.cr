module Game
  extend self

  @@state = :gameplay

  @@input = Input.new
  @@abstraction = Abstraction.new
  @@presentations = {
    gameplay: Presentation::Gameplay.new
  }


  def start
  end

  def loop
    loop do
      input.update
      abstraction.update
      presentation.update

      break if Terminal.quit?
    end
  end

  def finish
  end


  def state
    @@state
  end

  def input
    @@input
  end

  def abstraction
    @@abstraction
  end

  def presentation
    @@presentations[state]
  end
end