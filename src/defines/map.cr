class ECS::Map
  private def initialize_cell_types
    @types[:none] = CellType.new

    cell_type :grass,
      chars: ['·', '∙'],
      colors: [
        "#b0f0b0",
        "#90f090",
        "#70f070"
      ]

    cell_type :wall,
      chars: ['#', '╬'],
      colors: [
        "#c0c0c0",
        "#a0a0a0",
        "#808080"
      ]

    cell_type :cave_floor,
      chars: ['·'],
      colors: [
        "#4d3d2d",
        "#5d402d",
        "#4d3d30"
      ]

    cell_type :cave_wall,
      chars: ['#'],
      colors: [
        "#402010",
        "#401000",
        "#402000"
      ]

    cell_type :ship_floor,
      chars: [0xA0.chr],
      colors: [
        "#ff8866"
      ]

    cell_type :ship_wall,
      chars: ['█'],
      colors: [
        "#cf3f0f"
      ]
  end

  cell_prop collidable,
    :none,
    :wall,
    :ship_wall

  cell_prop opaque,
    :none,
    :wall

  cell_prop walkable,
    :grass,
    :cave_floor,
    :cave_wall,
    :ship_floor
end
