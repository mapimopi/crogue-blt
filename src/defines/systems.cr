class Abstraction
  def initialize_systems
    ecs_systems \
      Abstraction::System::PlayerMovement,
      Abstraction::System::CameraFollow,
      Abstraction::System::FovCalculation
  end
end
