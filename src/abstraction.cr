class Abstraction < Engine::Abstraction
  include ECS

  getter \
    camera : Camera = Camera.new


  def initialize
    initialize_systems
    initialize_map MapGenerator::BaseFloor.generate

    spawn_player
  end

  def update
    debug

    update_systems Game.state
  end


  private def debug
    @map.from MapGenerator::BaseFloor.generate if Game.input.action == :reset_map
  end
end
