class Input < Engine::Input
  getter \
    action : Symbol = :none

  def initialize
  end

  def update
    key = Terminal.input

    if key.nil?
      @action = :none
      return
    end

    @action =
      case key
      when BLT::TK::H, BLT::TK::KP_4 then :go_w
      when BLT::TK::J, BLT::TK::KP_2 then :go_s
      when BLT::TK::K, BLT::TK::KP_8 then :go_n
      when BLT::TK::L, BLT::TK::KP_6 then :go_e
      when BLT::TK::Y, BLT::TK::KP_7 then :go_nw
      when BLT::TK::U, BLT::TK::KP_9 then :go_ne
      when BLT::TK::B, BLT::TK::KP_1 then :go_sw
      when BLT::TK::N, BLT::TK::KP_3 then :go_se

      when BLT::TK::R then :reset_map # debug

      else :none
      end
  end
end
