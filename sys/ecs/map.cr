class ECS::Map
  @types = {} of CellVariant => CellType
  @variations = {} of CellKind => Array(CellVariant)
  @entities = [] of Array(Entity)

  getter \
    width = 0,
    height = 0

  @cells = [] of Cell


  def initialize(@width : Int32 = MAP_WIDTH, @height : Int32 = MAP_HEIGHT)
    initialize_cell_types
    initialize_cells
    initialize_entities
  end

  def from(blueprint)
    @width    = blueprint.width
    @height   = blueprint.height
    @cells    = [] of Cell
    @entities = [] of Array(Entity)

    @width.times do |x|
      @height.times do |y|
        @cells.push \
          Cell.new \
            type: variation(blueprint.cells[ index_at x, y])

        @entities.push [] of Entity
      end
    end
  end

  private def initialize_cells
    @width.times do |x|
      @height.times do |y|
        @cells.push \
          Cell.new \
            type: variation([:grass, :grass, :grass, :grass, :wall].sample)
      end
    end
  end

  private def initialize_entities
    @width.times do |x|
      @height.times do |y|
        @entities.push [] of Entity
      end
    end
  end

  private def variation(type)
    return type if type == :none
    @variations[type].sample
  end


  def cell_color(x, y) : UInt32
    cell_type_at(x, y).color
  end

  def cell_char(x, y) : Char
    cell_type_at(x, y).char
  end

  def cell_fov(x, y) : Bool
    if in_bounds? x, y
      cell_at(x, y).fov
    else
      false
    end
  end

  def cell_seen(x, y) : Bool
    if in_bounds? x, y
      cell_at(x, y).seen
    else
      false
    end
  end

  def entities_at(x, y) : Array(Entity)
    if in_bounds? x, y
      @entities[ index_at x, y ]
    else
      [] of Entity
    end
  end


  def cell_see(x, y)
    if in_bounds? x, y
      cell_at(x, y).fov  = true
      cell_at(x, y).seen = true
    end
  end

  def cell_unsee(x, y)
    if in_bounds? x, y
      cell_at(x, y).fov = false
    end
  end

  def cell_unsee!
    @width.times do |x|
      @height.times do |y|
        cell_at(x, y).fov = false
      end
    end
  end

  def entity_add(entity, x, y)
    if in_bounds? x, y
      @entities[ index_at x, y ].push entity
      entities_sort x, y
    else
      raise "Tried to add entity outside map bounds"
    end
  end

  def entity_remove(entity, x, y)
    if in_bounds? x, y
      @entities[ index_at x, y ].delete entity
    else
      raise "Tried to remove entity outside map bounds"
    end
  end

  def entity_move(entity, old_x, old_y, new_x, new_y)
    if in_bounds?(old_x, old_y) &&
       in_bounds?(new_x, new_y)
      @entities[ index_at old_x, old_y ].delete entity
      @entities[ index_at new_x, new_y ].push entity
      entities_sort new_x, new_y
    else
      raise "Tried to move entity outside map bounds"
    end
  end

  private def entities_sort(x, y)
    @entities[ index_at x, y ].sort! do |a, b|
      priority_a =
        if a.has? :glyph
          a.glyph.priority
        else
          0
        end

      priority_b =
        if b.has? :glyph
          b.glyph.priority
        else
          0
        end

      priority_a <=> priority_b
    end
  end


  private def in_bounds?(x, y) : Bool
    x >= 0 && x < @width &&
    y >= 0 && y < @height
  end

  private def cell_type_at(x, y) : CellType
    if in_bounds? x, y
      @types[ cell_at(x, y).type ]
    else
      @types[ :none ]
    end
  end

  private def cell_at(x, y) : Cell
    @cells[ index_at x, y ]
  end

  private def index_at(x, y) : Int32
    x * @width + y
  end


  macro cell_type(name, chars, colors)
    @variations[{{ name }}] = [] of Symbol

    {% for char, i in chars %}
      {% for color, j in colors %}
        @types[ {{ name }}_{{ i * colors.size + j }} ] = CellType.new \
          name: {{ name }},
          char: {{ char }},
          color: ( {{ color.gsub(/\#/, "").to_i 16 }} + 0xff000000_u32 ).to_u32

        @variations[{{ name }}].push {{ name }}_{{ i * colors.size + j }}
      {% end %}
    {% end %}
  end

  macro cell_prop(name, *types)
    @cell_prop_types_{{ name }} = Set{{ types }}
    def cell_{{ name }}?(x, y)
      @cell_prop_types_{{ name }}.includes? cell_type_at(x, y).name
    end
  end
end
