alias CellKind = Symbol    # :grass
alias CellVariant = Symbol # :grass_0, :grass_1

class ECS::Cell
  property \
    type : CellVariant = :none,
    fov = false,
    seen = false


  def initialize(@type); end
end
