class ECS::MapBlueprint
  property \
    cells = [] of Symbol,
    entities = [] of Symbol

  getter \
    width = MAP_WIDTH,
    height = MAP_HEIGHT,
    size = 0


  def initialize(@width, @height, cell_type = :none)
    @size = @width * @height
    @size.times do
      @cells.push cell_type
    end
  end

  def put(x, y, cell_type)
    @cells[ index_at x, y ] = cell_type
  end

  def see(x, y)

  end

  def index_at(x, y)
    x * @width + y
  end
end
