module ECS
  getter \
    entities = [] of Entity,
    systems = [] of System,
    map = Map.new


  def initialize_map(blueprint)
    @map.from blueprint
  end

  def update_systems(state : Symbol)
    @systems.each do |system|
      next unless system.active?
      next unless system.state == state

      case system.type
      when :entity
        @entities.each do |entity|
          if entity.has? system.required_components
            system.update entity
          end
        end
      when :global
        system.update
      end
    end
  end


  def spawn_at(x, y)
    entity = Entity.new(self, x, y)
    @entities << entity
    @map.entity_add(entity, x, y)
    return entity
  end


  macro ecs_systems(*systems)
    {% for system in systems %}
      @systems << {{ system }}.new(self)
    {% end %}
  end
end
