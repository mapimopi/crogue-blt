class ECS::Entity
  getter \
    x = 0,
    y = 0

  @world : ECS
  @have = Set(Symbol).new
  @components = {} of Symbol => Component


  def initialize(@world, @x, @y); end

  def has?(check : Set(Symbol))
    check.subset? @have
  end

  def has?(*check : Symbol)
    has? check.to_set
  end

  def add(name, component : Component? = nil)
    raise "Component #{name.to_s} already exists for this entity" if @have.includes? name

    @have << name
    @components[name] = component unless component.nil?
  end

  def move_to(x, y)
    @world.map.entity_move self,
      old_x: @x,
      old_y: @y,
      new_x: x,
      new_y: y

    @x = x
    @y = y
  end

  def move_by(x, y)
    move_to @x + x, @y + y
  end


  macro ecs_components(*components)
    {% for component in components %}
      def {{ component.names.last.underscore }}
        @components[:{{ component.names.last.underscore }}].as({{ component }})
      end
    {% end %}
  end
end
