abstract class ECS::System
  getter \
    state : Symbol = :none,
    type : Symbol = :entity,
    required_components : Set(Symbol) = Set(Symbol).new


  def initialize(@abstraction : Engine::Abstraction); end

  def update(); end
  def update(entity : Entity); end

  @is_active = true
  def active?; @is_active end
  def enable;  @is_active = true end
  def disable; @is_active = false end


  # TODO: A way for a system to emit events and subscribe to events.
  # One central event system should be stored inside World, which
  # should call all subscribed event observers callbacks.

  macro state(new_state)
    @state = {{ new_state }}
  end

  macro type(new_type)
    @type = {{ new_type }}
  end

  macro filter(*component_names)
    getter required_components = Set{{ component_names }}
  end
end

