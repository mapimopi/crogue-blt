class ECS::CellType
  property \
    name : Symbol = :none,
    char : Char = ' ',
    color : BLT::Color = 0_u32


  def initialize(@name = :none, @char = ' ', @color = 0_u32); end
end
