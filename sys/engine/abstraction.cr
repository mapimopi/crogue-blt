abstract class Engine::Abstraction
  def update; end
  def destroy; end
end
