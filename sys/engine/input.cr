abstract class Engine::Input
  def update; end
  def destroy; end
end
