abstract class Engine::Presentation
  def update; end
  def destroy; end
end
