module M
  extend self

  # inclusive
  def random(min : Int32, max : Int32)
    diff = max - min + 1
    (rand * diff).floor.to_i + min
  end

  # exclusive
  def random(max : Int32)
    (rand * max).floor.to_i
  end
end
