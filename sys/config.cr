module Terminal
  INIT_WIDTH  = 64
  INIT_HEIGHT = 48
  INIT_TITLE  = "Hello, World!"
  TILE_FONT   = "tileset16.png"
  TILE_WIDTH  = 16
  TILE_HEIGHT = 16
  TEXT_FONT   = "tileset8x16.png"
  TEXT_WIDTH  = 8
  TEXT_HEIGHT = 16
end

MAP_WIDTH  = 256
MAP_HEIGHT = 256
