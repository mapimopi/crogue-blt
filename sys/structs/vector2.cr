struct Vector2
  property \
    x : Int32 = 0,
    y : Int32 = 0

  def initialize(@x = 0, @y = 0); end

  def +(other)
    Vector2.new x + other.x, y + other.y
  end

  def -(other)
    Vector2.new x - other.x, y - other.y
  end

  def *(other)
    Vector2.new x * other.x, y * other.y
  end

  def /(other)
    Vector2.new x / other.x, y / other.y
  end
end
