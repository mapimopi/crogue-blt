struct Vector2f
  property \
    x : Float32 = 0.0,
    y : Float32 = 0.0

  def initialize(@x = 0.0, @y = 0.0); end

  def +(other)
    Vector2f.new x + other.x, y + other.y
  end

  def -(other)
    Vector2f.new x - other.x, y - other.y
  end

  def *(other)
    Vector2f.new x * other.x, y * other.y
  end

  def /(other)
    Vector2f.new x / other.x, y / other.y
  end
end
