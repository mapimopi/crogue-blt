module Terminal
  extend self
  extend BLTHelper

  enum Layer
    Tiles
    Entities
    UI_BG
    UI
  end

  TEXT_OFFSET_X = TEXT_WIDTH - TILE_WIDTH
  TEXT_OFFSET_Y = (TILE_HEIGHT - TEXT_HEIGHT) / 2
  TILE_TEXT_RATIO_X = TILE_WIDTH.to_f / TEXT_WIDTH.to_f
  TILE_TEXT_RATIO_Y = TILE_HEIGHT.to_f / TEXT_HEIGHT.to_f

  @@current_layer = Layer::Tiles
  @@default_foreground_color : BLT::Color = color_from_argb 255, 255, 255, 255
  @@default_background_color : BLT::Color = color_from_argb 255, 6, 8, 14
  @@default_text_color : BLT::Color = BLT.color_from_name "white"

  def open
    BLT.open
    BLT.set "
      window:
        size=#{ INIT_WIDTH }x#{ INIT_HEIGHT },
        title='#{ INIT_TITLE }',
        resizeable=true;

      input:
        filter=[keyboard, mouse, properties];

      font:
        #{ image_path TILE_FONT },
        size=#{ TILE_WIDTH }x#{ TILE_HEIGHT },
        codepage=437;

      text font:
        #{ image_path TEXT_FONT },
        size=#{ TEXT_WIDTH }x#{ TEXT_HEIGHT },
        codepage=437;
    "

    BLT.refresh
  end

  def close
    BLT.close
  end

  def width
    BLT.state BLT::State::WIDTH
  end

  def height
    BLT.state BLT::State::HEIGHT
  end

  def mouse_x
    BLT.state BLT::TK::MOUSE_X
  end

  def mouse_y
    BLT.state BLT::TK::MOUSE_Y
  end

  def input : BLT::TK | Nil
    if BLT.has_input?
      BLT.read
    else
      nil
    end
  end

  def quit?
    BLT.peek == BLT::TK::CLOSE
  end

  private def assets_path
    File.expand_path "assets"
  end

  private def image_path(filename)
    File.join assets_path, "images", filename
  end

  def refresh; BLT.refresh end

  def layer(layer)
    @@current_layer = layer
    BLT.layer layer
  end

  def clear
    BLT.bkcolor @@default_background_color
    BLT.clear
  end

  def put(x, y, c : Int32)
    BLT.put x, y, c
  end

  def put(x, y, c : Char)
    BLT.put x, y, c.ord
  end

  def put(x, y, c : Int32, color : BLT::Color)
    BLT.color color
    BLT.put x, y, c
  end

  def put(x, y, c : Char, color : BLT::Color)
    BLT.color color
    BLT.put x, y, c.ord
  end

  def put(x, y, c : Int32, foreground : BLT::Color, background : BLT::Color)
    BLT.color foreground
    BLT.bkcolor background
    BLT.put x, y, c
  end

  def put(x, y, c : Char, foreground : BLT::Color, background : BLT::Color)
    BLT.color foreground
    BLT.bkcolor background
    BLT.put x, y, c.ord
  end

  def fg_color(name : String)
    BLT.color BLT.color_from_name name
  end

  def fg_color(color : BLT::Color)
    BLT.color color
  end

  def bg_color(name : String)
    BLT.bkcolor BLT.color_from_name name
  end

  def bg_color(color : BLT::Color)
    BLT.bkcolor color
  end

  def reset_bg_color
    BLT.bkcolor @@default_background_color
  end

  def default_fg_color
    @@default_foreground_color
  end

  def default_bg_color
    @@default_background_color
  end

  def get_char(x : Int32, y : Int32)
    BLT.pick x, y, @@current_layer
  end

  def get_fg_color(x : Int32, y : Int32)
    BLT.pick_color x, y, @@current_layer
  end

  def get_bg_color(x : Int32, y : Int32)
    BLT.pick_bkcolor x, y
  end

  def print_color(color : BLT::Color)
    "[color=##{ color.to_s 16 }]"
  end

  def print_offset(ox : Int32, oy : Int32)
    "[offset=#{ ox },#{ oy }]"
  end

  def print_text(x, y, string : String, color : BLT::Color = @@default_text_color)
    print_text_background x, y, string
    print_text_foreground x, y, string, color
  end

  private def print_text_background(x, y, string)
    BLT.layer Layer::UI_BG
    BLT.color @@default_background_color

    length = ( string.size / TILE_TEXT_RATIO_X ).ceil.to_i
    text = "█" * length # ▓

    print x, y, text
  end

  private def print_text_foreground(x, y, string, color)
    BLT.layer Layer::UI
    BLT.color color

    text = ""
    string.chars.each_with_index do |char, index|
      text = "#{ text }[offset=#{ -5 + TEXT_OFFSET_X * index },#{ TEXT_OFFSET_Y }]#{ char }"
    end

    print x, y, "[font=text]#{ text }[/font]"
  end
end
